package src.main.java.JavaBasics1;

public class Boolean {
    public static void main(String[] args) {
        int a = 0;
//        boolean b = true;
//        boolean b = a != 0;
        boolean b = a == 0;
        System.out.println(b);

        if (true) {
            System.out.println("We are in the if statement");
        }
///////////////////////////

        boolean passedDoor = true;
        boolean missedDoor = false;
        boolean passedAllDoors = false;
        int doorCount = 0;

        if (passedDoor) {
            System.out.println("We passed the 1st door");
            doorCount = doorCount + 1;
        }
        if (missedDoor) {
            System.out.println("We passed the 2nd door");
            doorCount = doorCount + 1;
        }
        if (passedDoor) {
            System.out.println("We passed the 3rd door");
            doorCount = doorCount + 1;
        }
        System.out.println("We passed " + doorCount + " doors");

        if (doorCount == 3) {
            passedAllDoors = true;
        }
        if (passedAllDoors) {
            System.out.println("Congrats, you won the program");
        }
    }
}
