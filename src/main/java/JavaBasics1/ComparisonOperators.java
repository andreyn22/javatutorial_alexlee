package JavaBasics1;

public class ComparisonOperators {
    public static void main(String[] args) {
        int a = 4;
        int b = 5;
        boolean c = (a == b);
        boolean d = (a != b);
        boolean e = (a <= b);
        boolean f = (a >= b);

        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);

        double x = 2.4;
        if (x == 2.4) {
            System.out.println("They are equal!");
        }
        if (x > 7) {
            System.out.println("Greater than 7");
        }
        if (x == 2.4 || x == 2.5) {
            System.out.println("x is 2.4 or 2.5");
        }
        if (x>=0 && x<=100){
            System.out.println("x is in range");
        }
    }
}
