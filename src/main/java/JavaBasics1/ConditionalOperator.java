package JavaBasics1;

public class ConditionalOperator {
    public static void main(String[] args) {
        int a = (7 > 3) ? 7 : 3;
        // is 7 greater than 3?
        // if true, then will store it(7 to "a";
        // if false, then will store 3 to "a";
        System.out.println(a);

        if (7 > 3) {
            a = 7;
        } else {
            a = 3;
        }
        System.out.println(a);

        String b = "hello";
        String c = "bau";
        double result1 = b.equals("hello") ? 0.5 : 0.25;
        System.out.println(result1);
        double result2 = c.equals("hello") ? 0.5 : 0.25;
        System.out.println(result2);
    }
}
