

public class DoubleLongFloat {
    public static void main(String[] args) {
        int a = 999999999;            // whole number (not a fractional number) that can be positive, negative, or zero
        long b = 999999999999999999L;  //a bit integer a 64-bit two's complement integer
        float c = 2.5f;              //data type that can store fractional numbers from 3.4e−038 to 3.4e+038.
        double d = 5.4444444;        //long float

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(a);
    }
}
