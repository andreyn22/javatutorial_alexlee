package JavaBasics1;

public class Exponents {
    public static void main(String[] args) {
        int a = 5;
        System.out.println(5 * 5 * 5 * 5);
        System.out.println(Math.pow(5, 4));
        System.out.println((int) Math.pow(5, 4));
        System.out.println();
        double exponents = Math.pow(2, 3);
        System.out.println(exponents);
    }
}
