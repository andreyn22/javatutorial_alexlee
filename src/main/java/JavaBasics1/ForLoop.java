package JavaBasics1;

public class ForLoop {
    public static void main(String[] args) {
        System.out.println("I love ducks!");
        for (int i = 0; i < 4; i++) {
            System.out.println("I love dogs");
        }

        int[] grades = {98, 100, 83, 90, 93};
        for (int i = 0; i < grades.length; i++) {
            System.out.println(grades[i]);
        }
    }
}
