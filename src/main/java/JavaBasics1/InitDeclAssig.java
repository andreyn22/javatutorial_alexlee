package JavaBasics1;

// InitDeclAssig = Initialization, Declaration, Assignement

public class InitDeclAssig {
    public static void main(String[] args) {
        String pizza1 = "pineapple"; // initialization;
        String pizza2;               // declaration;
        pizza2 = "bbq";              // assignment;
    }
}
