package JavaBasics1;

public class Logic {
    public static void main(String[] args) {
//        ||   or
//        &&   and
//        !    not
        boolean a = true || true;
        boolean b = !false;
        boolean c = !(true || false);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        System.out.println();

        int d = 3;
        int e = 4;
        boolean f = d > e;
        boolean g = (d < e) && true;
        boolean h = (d < e) || true;
        System.out.println(f);
        System.out.println(g);
        System.out.println(h);

        System.out.println();

        boolean isSunny = true;
        boolean iAmHappy = true; // if one is false, will be printed the bellow message
        if ((d < e && isSunny) || iAmHappy) {
            System.out.println("It's gonna be a good day!");
        }
    }
}
