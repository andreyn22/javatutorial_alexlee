package JavaBasics1;

public class MainMethod {
    public static void main(String[] args) {
        sayHi();
        saySomething("Hey!");

        String[] a = {"Elem1", "Elem2"};
        saySomethingnew(a);
    }

    static void sayHi() {
        System.out.println("Hi!");
        // will not print anything if nothing is typed in main
        // will be printed if we call static method to main
    }

    static void saySomething(String s) {  //string
        System.out.println();
        System.out.println(s);
    }

    static void saySomethingnew(String[] a) { //array
        System.out.println();
        System.out.println(a[0]);
        System.out.println(a[1]);
    }
}
