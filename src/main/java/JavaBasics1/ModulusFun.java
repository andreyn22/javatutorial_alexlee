package JavaBasics1;

public class ModulusFun {
    public static void main(String[] args) {
        System.out.println(4 + 2);
        System.out.println(4 % 2);  // the rest
        System.out.println(4 % 3);  // the rest
        System.out.println(1.5 % 0.5);  // the rest
        System.out.println(1 % 0.6);  // the rest
    }
}
