package JavaBasics1;

import java.util.Scanner;

public class NamingConventions {
    public static void main(String[] args) {
        int a = 5; // for math stuff
        int ageOfStudent = 25;
        int MAX_AGE = 100; // constant variables, all capital + final keyword(never will not be changed) + static;
        Scanner scanner = new Scanner(System.in); // scanner object
    }

    public static int addTwoNumbers(int a, int b) {
        return 0;
    }
}
