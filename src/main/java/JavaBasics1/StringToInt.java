package JavaBasics1;

public class StringToInt {
    public static void main(String[] args) {
        String s = "102";
        System.out.println(s + 4);

//        Integer.parseInt(s);
//        System.out.println(Integer.parseInt(s) + 4);

//or

//        int n = Integer.parseInt("102");
//        System.out.println(n + 4);


        int n = 8;
        System.out.println(n + 1);
        System.out.println(Integer.toString(n) + 1); //parse integer to string

    }
}
