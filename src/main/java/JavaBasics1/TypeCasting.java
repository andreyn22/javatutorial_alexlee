package JavaBasics1;

public class TypeCasting {
    public static void main(String[] args) {
        int a = 0;
        int b = (int) 0.5; // takes 0.5, converts to integer and stores into an int
        int c = (int) 2.5;
        System.out.println(a);

        byte d;
        short e;
        char f;
        int g = 500;
        long h;
        float i;
        double j = 500.1;
        double ex = g;
    }
}

