package JavaBasics1;

import java.util.ArrayList;
import java.util.Scanner;

public class WhileLoop {
    public static void main(String[] args) {

//        while(true){
//            System.out.println("a"); // this will return an infinite loop of a
//        }
//////////////////////
        int a = 0;
        while (a < 10) {
            System.out.println("a is less than 10: " + a);
            a++;
        }
//////////////////////
        while (a == 1) {
            System.out.println("a");// not printed because condition is not met // if "a==0" => infinite "a" loop
        }
//////////////////////
        do {
            System.out.println("a"); // printed before checking the condition
        } while (a == 1);
//////////////////////
        System.out.println();
        String sentence = "hjsscjksdnc are awesome!";
        Scanner scan = new Scanner(sentence);
        ArrayList<String> words = new ArrayList<String>();

        while (scan.hasNext()) {
            words.add(scan.next());
        }
        System.out.println(words);
    }
}
