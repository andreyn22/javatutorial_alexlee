package JavaBasics2;

public class Account {
    String name;
    int age;

    public static void main(String[] args) {

        Account a = new Account();

        a.setName("Pablo");
        a.setAge(34);
//        System.out.println(a.getName());
//        System.out.println(a.getAge());

        a.printDetails();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name; // this.Account name = name
    }

    public Integer getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void printDetails() {
        System.out.println(getName() + ", " + getAge());
    }
}
