package JavaBasics2;

import java.util.ArrayList;

public class ArrayListProj {
    public static void main(String[] args) {
        String[] fruits = new String[3]; // regular Array
        fruits[0] = "Mango";
        fruits[1] = "Apple";
        fruits[2] = "Strawberry";
//        fruits[3] = "Cranberry"; // not flexible, can be extended only after changing the array size
        System.out.println(fruits[1]);

        ArrayList fruitList = new ArrayList(); // Array List
        fruitList.add("Mango");
        fruitList.add("Apple");
        fruitList.add("Strawberry");
        fruitList.add("Cranberry"); // flexible, can be added any other items
        System.out.println(fruitList);
        System.out.println("----------");
        fruitList.remove("Strawberry");
        System.out.println(fruitList);
    }
}
