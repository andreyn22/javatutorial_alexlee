package JavaBasics2;

public class Break {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Hi");
            break;
        }

        System.out.println("----------------------");

        int i = 0;
        while (i < 3) {
            System.out.println("Hello");
//            break;
            i++;
            break;
        }

        System.out.println("----------------------");

        int[] numbers = {10, 20, 30, 40, 50};
        for (int m = 0; m < numbers.length; m++) {
            if (numbers[i] == 30) {
                break;
            }
            System.out.println(numbers[m]);
        }

        System.out.println("----------------------");

        int j = 4;
        switch (j) {
            case 0:
                System.out.println("Zero");
                break;
            case 1:
                System.out.println("One");
                break;
            default:
                System.out.println("Others");
                break;
        }

        System.out.println("----------------------");

        for (int k = 0; k < 5; k++) {
            for (int l = 0; l < 3; l++) {
                System.out.println(k + ", " + l);
                break;
            }
        }
    }
}
