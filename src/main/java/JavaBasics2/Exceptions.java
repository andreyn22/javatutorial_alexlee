package JavaBasics2;

import java.util.ArrayList;

public class Exceptions {
    public static void main(String[] args) {
        String[] pets = {"dog", "cat", "monkey"};
//        System.out.println(pets[3]);

//        Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 3 out of bounds for length 3
//        at JavaBasics2.Exceptions.main(Exceptions.java:6)

//        int i = "alex"; //java: incompatible types: java.lang.String cannot be converted to int

        ArrayList<String> list = new ArrayList<String>();
        list.add("book");
        list.add("pen");
        System.out.println(list.get(0));
        System.out.println(list.get(1));

    }
}
