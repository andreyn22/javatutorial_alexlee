package JavaBasics2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileReader {
    public static void main(String[] args) throws IOException {
//        http://textfiles.com/100/captmidn.txt
        File file = new File("C:/Users/anegura/OneDrive - ENDAVA/Documents/captmidn.txt");
        Scanner scanner = new Scanner(file);
//        System.out.println(scanner.nextLine());// scan the 1st line

//        while (scanner.hasNextLine()) {          // scan the all lines while they exists
//            System.out.println(scanner.nextLine());
//        }

        String fileContent = "";
        while (scanner.hasNextLine()) {
            fileContent = fileContent.concat(scanner.nextLine() + "\n");
        }

        FileWriter writer = new FileWriter("C:/Users/anegura/OneDrive - ENDAVA/Documents/newfilereproduced1.text");
        writer.write((fileContent));
        writer.close();
    }
}
