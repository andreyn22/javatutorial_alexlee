package JavaBasics2;

public class FinallyKeyword1 {
    public static void main(String[] args) {
        int a = 5;
        int b = 0;
        try {
            int c = a / b;
            System.out.println(c);
        } catch (Exception e) {
            System.out.println(e); //java.lang.ArithmeticException: / by zero
        } finally {
            System.out.println("This is in 'finnaly', it always gets run!");
        }
    }
}
