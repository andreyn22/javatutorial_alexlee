package JavaBasics2;

import java.util.ArrayList;

public class ForEach {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {   // classic for loop
            System.out.println(i);
        }
        System.out.println("------------");

        String[] cars = {"BMW M2", "Veloster N", "GTI"};
        for (String car : cars) {    // for each loop -  simplified because depends on an array (cars)
            System.out.println(car);
        }
        System.out.println("------------");

        char[] characters = {'a', 'b', 'c'};
        for (char character : characters) { // for each loop for characters
            System.out.println(character);
        }
        System.out.println("------------");

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(3);
        numbers.add(2);
        numbers.add(1);

        System.out.println("Printing the arraylist with for each loop");
        for (int a : numbers) {      // for each loop
            System.out.println(a);
        }

        System.out.println("Printing the arraylist with for loop");
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }
    }
}
