package JavaBasics2;

import java.util.HashMap;

public class HashMap2 {
    public static void main(String[] args) {
        HashMap fun = new HashMap<String, String>();
        fun.put("ngrx", "Coco99");
        fun.put("gogu", "Yoga69");
        fun.put("coco", "relax69");

//      Printing all the elements of HashMap:
        System.out.println(fun);
        System.out.println("------------------");

//      Removing the element with key 'ngrx':
        fun.remove("ngrx");
        System.out.println(fun);
        System.out.println("------------------");

//      Validating existence of a value:
        System.out.println(fun.containsValue("Coco99"));
        System.out.println(fun.containsValue("relax69"));
        System.out.println(fun.containsValue("Coco92"));
        System.out.println("------------------");

//      Validating existence of a key:
        System.out.println(fun.containsKey("coco"));
        System.out.println(fun.containsKey("moco"));
        System.out.println("------------------");

//      Retrieving size:
        System.out.println(fun.size());
        System.out.println("------------------");

//      Replacing a value:
        fun.replace("gogu", "YOGA69");
        System.out.println(fun);
        System.out.println("------------------");

//      Getting values:
        System.out.println(fun.values());
        System.out.println("------------------");

//      Getting keys:
        System.out.println(fun.keySet());
        System.out.println("------------------");
    }
}
