package JavaBasics2;

import java.util.HashSet;

public class HashSet1 {
    public static void main(String[] args) {
        int[] array = {3, 5, 1}; // simple array
        System.out.println(array[0]);

//        HashSet hashSet = new HashSet<>(); // hashset
        HashSet<String> h = new HashSet<String>();
        h.add("lemur");
        h.add("orangatang");
        h.add("spider monkey");


        System.out.println(h);

        h.remove("lemur");
        System.out.println(h);

        h.clear();
        System.out.println(h);
        System.out.println(h.isEmpty());

        h.add("gorilla");
        System.out.println(h);

        h.add("spider monkey");
        System.out.println(h);
    }
}
