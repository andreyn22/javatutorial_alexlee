package JavaBasics2;

import java.util.HashSet;
import java.util.Iterator;

public class HashSet2 {
    public static void main(String[] args) {
        HashSet<Integer> hashthings = new HashSet<Integer>();
        hashthings.add(13);
        hashthings.add(24);
        hashthings.add(5);

        System.out.println(hashthings);
        System.out.println(hashthings.toArray());

        Object[] h = hashthings.toArray();
        System.out.println(h[0]);// randomly takes the value from the list, does not take into account the index

        Iterator<Integer> it = hashthings.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
