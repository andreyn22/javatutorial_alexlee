package JavaBasics2;

public class MathIsFun {
    public static void main(String[] args) {
        System.out.println(Math.class);
        System.out.println(Math.E);
        System.out.println(Math.PI);
        System.out.println(Math.abs(-12)); // absolute value
        System.out.println(Math.acos(.54)); //arccosin
        System.out.println(Math.atan2(6, 3));
        System.out.println(Math.ceil(2.3));
        System.out.println(Math.copySign(4, -2));
        System.out.println(Math.cosh(-0.2));
        System.out.println(Math.decrementExact(9));
        System.out.println(Math.expm1(2));
        System.out.println(Math.pow(2, 3));
        System.out.println(Math.sqrt(9));
        System.out.println(Math.subtractExact(9, 2));
        System.out.println(Math.toIntExact((long) 6.4));

    }
}
