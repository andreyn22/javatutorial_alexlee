package JavaBasics2;

public class MethodParams {
    public static void main(String[] args) {
        saySomething("Alex");
        saySomething("Microphone");
        saySomething("laptop");

        System.out.println("------------");

        printInfo("Alex", 23);
        printInfo("Ana", 24);
        printInfo("Aisha", 25);

        System.out.println("------------");

        int result1 = sum(2, 4);
        int result2 = sum(3, 5);
        int result3 = sum(4, 7);
        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);
    }

    public static void saySomething(String s) {
        System.out.println(s);
    }

    public static void printInfo(String name, int age) {
        System.out.println(name + " is " + age + " years old");
    }

    public static int sum(int x, int y) {
        return x + y;
    }

}
