package JavaBasics2;

public class Methods {
    public static void main(String[] args) {
//        int a = 5;
//        int b = 10;
//        System.out.println(a * b);
//
//        int c = 2;
//        int d = 3;
//        System.out.println(c * d);
//
//        int e = 3;
//        int f = 4;
//        System.out.println(e * f);

        welcome();
        multiply(5, 10);
        multiply(2, 3);
        multiply(3, 4);
        divide(20, 5);
        divide(22, 5);
        divide(25, 5);

    }

    public static void welcome() {
        System.out.println("Welcome to our calculator:");
    }

    public static void multiply(int a, int b) {
        System.out.println(a * b);
    }

    public static void divide(double a, double b) {
        System.out.println(a / b);
    }

}
