package JavaBasics2;

import java.util.ArrayList;

public class NullKeyword {
    public static void main(String[] args) {
        String a = "hey, whatsup?";
        String b = null;
        ArrayList c = null;

        int m = 4;
        double n = 7.1;

        if (b == null) {
            System.out.println("is null");
        }
    }
}
