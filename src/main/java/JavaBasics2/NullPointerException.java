package JavaBasics2;

import java.util.Scanner;

public class NullPointerException extends Throwable {
    public static void main(String[] args) {
//        String a = "monkey";
        String a = null;
        System.out.println(a.length());

//        Exception in thread "main" java.lang.NullPointerException: Cannot invoke "String.length()" because "a" is null
//        at JavaBasics2.NullPointerException.main(NullPointerException.java:7)

//        int i = null; // null cannot be assigned to primitives
//        double
//        long
//        float

        String b = null; // null can be assigned to objects
        Scanner scan = null;
        scan.nextInt();
    }
}
