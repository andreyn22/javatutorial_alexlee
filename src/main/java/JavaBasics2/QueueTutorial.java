package JavaBasics2;

import java.util.LinkedList;
import java.util.Queue;

public class QueueTutorial {
    public static void main(String[] args) {
        Queue<String> bbqLine = new LinkedList<String>();
        bbqLine.add("Jackson");
        bbqLine.add("Tyreek");
        bbqLine.add("Susan");

        System.out.println(bbqLine.poll()); // take first element out of line, remaining 2 of 3
        System.out.println(bbqLine);
        System.out.println("----------");

        System.out.println(bbqLine.peek()); // take aside the first element, but is still on line, remaining 2of2
        System.out.println(bbqLine);
        System.out.println("----------");

        Queue<String> q = new LinkedList<String>();
        q.add("A");
        q.add("B");
        q.add("C");

        System.out.println(q.size());
        System.out.println(q.contains("G"));
        System.out.println(q.contains("C"));
        System.out.println(q.toArray()[2]);

        System.out.println(q); // entire queue
        System.out.println(q.poll()); // taking aside first element 'C'
        System.out.println(q); // entire queue remaining


    }
}
