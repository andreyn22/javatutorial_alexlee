package JavaBasics2;

public class ReturnStatements {
    public static void main(String[] args) {
        printAMessage();
        add1(2, 3);

        int sum2 = add2(3, 4);
        System.out.println(sum2);

        String shouting = caps("why are you reading my dirary mom ?!");
        System.out.println(shouting);

        int[] awesomeArray = gimmeArrayFromInts(3, 7, 1);
        System.out.println(awesomeArray[0]);
        System.out.println(awesomeArray[1]);
        System.out.println(awesomeArray[2]);
    }

    public static void printAMessage() {
        System.out.println("This is our first method!");
    }

    public static void add1(int a, int b) {
        System.out.println(a + b);
    }

    public static int add2(int c, int d) {
        return c + d;
    }

    public static String caps(String s) {
        return s.toUpperCase();
    }

    public static int[] gimmeArrayFromInts(int a, int b, int c) {
        int[] array = new int[3];
        array[0] = a;
        array[1] = b;
        array[2] = c;
        return array;
    }
}
