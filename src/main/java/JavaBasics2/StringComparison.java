package JavaBasics2;

public class StringComparison {
    public static void main(String[] args) {
        String a = new String("lemur");
        String b = new String("lemur"); // the same "lemur" is stored in different object b, other then a

        if (a.equals(b)) { //  for string object don't work "a == b"
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
