package JavaBasics2;

public class StringMethods {
    public static void main(String[] args) {
        String name = "Billy Bob Joe";
        System.out.println("Name: " + name);
        System.out.println("Length: " + name.length());
        System.out.println("Uppercase: " + name.toUpperCase());
        System.out.println("Lowercase: " + name.toLowerCase());
        System.out.println("First character: " + name.charAt(0)); //getting first character of a string
        System.out.println("Last char: " + name.charAt(12));
        System.out.println("Substring: " + name.substring(6, 13)); // cutting a part of a string
    }
}
