package JavaBasics2;

public class SwitchCaseString {
    public static void main(String[] args) {
        String dog = "pomeranian";

        switch (dog) {
            case "pomeranian":
                System.out.println("small dog");
                break;
            case "great dane":
                System.out.println("large dog");
                break;
            default:
                System.out.println("tru a different dog name");
        }
    }
}
