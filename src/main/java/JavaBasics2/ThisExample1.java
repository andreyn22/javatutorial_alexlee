package JavaBasics2;

public class ThisExample1 {
    int a;
    int b;

    public static void main(String[] args) {
        ThisExample1 t = new ThisExample1();
        t.setData(4, 3);
        System.out.println(t.a);
        System.out.println(t.b);
    }

    public void setData(int a, int b) {
        this.a = a; // this" refer to int a (to the top of class name
        this.b = b; // this will not work with static keyword
    }
}
