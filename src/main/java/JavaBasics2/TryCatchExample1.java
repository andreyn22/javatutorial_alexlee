package JavaBasics2;

import java.util.Scanner;

public class TryCatchExample1 {
    public static void main(String[] args) {
        try {
            int[] a = {4, 5, 1};
            System.out.println(a[3]);
        } catch (Exception e) {
            System.out.println("An exception happened! Try to check the index of the array");
        }
        System.out.println("-----------------");

        Scanner scan = new Scanner(System.in);
        System.out.println("What's your fav number?");
        try {
            int n = scan.nextInt();
            System.out.println(n);
        } catch (Exception e) {
            System.out.println("Sorry, please enter a number!");
        }
        System.out.println("-----------------");

    }
}

