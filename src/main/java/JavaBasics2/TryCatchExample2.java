package JavaBasics2;

public class TryCatchExample2 {
    public static void main(String[] args) {
        try {
//            int[] b = null;
            int[] b = {4};
            System.out.println(b[1]);
        } catch (java.lang.NullPointerException e) {
            System.out.println("Your array is null!");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Your index is out of bounds!");
        } catch (Exception e) {
            System.out.println("Something else went wrong!");
        }
    }
}

