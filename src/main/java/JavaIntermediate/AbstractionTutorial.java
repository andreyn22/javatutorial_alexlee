package JavaIntermediate;

interface DogInterface { // an interface assumes that all the methods are abstract, not implemented; just a list
    void bark();
    void poop();
}

abstract class Dog {// abstract classes are a bunch of variables and methods that can be used to create other classes
    public void bark() {
        System.out.println("Bark!"); // implemented method
    }

    public abstract void poop(); // abstract method is a method that isn't implemented yet
}

class Chihuahua extends Dog {
    public void poop() {
        System.out.println("Dog pooped!");
    }
}

public class AbstractionTutorial {
    public static void main(String[] args) {
        Chihuahua c = new Chihuahua();
        c.bark();
        c.poop();
    }
}
