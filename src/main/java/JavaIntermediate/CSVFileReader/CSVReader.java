package JavaIntermediate.CSVFileReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVReader {
    public static void main(String[] args) {
//        String path = "C:\\Users\\anegura\\OneDrive - ENDAVA\\Documents\\Custom Office Templates\\csvExample.csv";
        String path = "C:/Users/anegura/OneDrive - ENDAVA/Documents/Custom Office Templates/SalesJan2009.csv";
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                System.out.println("Transaction_date: " + values[0] + ", Payment_Type: " + values[3] + ", Country: " + values[7]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
