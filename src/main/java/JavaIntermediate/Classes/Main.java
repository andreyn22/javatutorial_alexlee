package JavaIntermediate.Classes;

public class Main {
    public static void main(String[] args) {
        Class1 c1 = new Class1(); // creating an object // object are made from the classes
        Class1 d1 = new Class1(); // two object from the class 1 blueprint
        System.out.println(c1.x);
        c1.printHi();

        Class2 c2 = new Class2();
        System.out.println(c2.y);
    }
}
