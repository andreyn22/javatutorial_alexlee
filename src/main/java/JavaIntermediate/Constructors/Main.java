package JavaIntermediate.Constructors;

public class Main {
    public static void main(String[] args) {
        Shirt s = new Shirt("White", 'M');
        // Shirt() is the default constructor // Constructor made the new object Shirt called "s"
        // A constructor is a method that makes an object
//        s.setColor("White");
//        s.setSize('M');
//        s.putON();

        System.out.println(s.color);
        System.out.println(s.size);

    }
}
