package JavaIntermediate.Constructors;

public class Shirt {
    public static String color;
    public static char size;

//    Shirt() {
//        System.out.println("inside constructor!");
//    } // making a constructor inside of a class

    Shirt() {
    } // default // not mandatory here

    Shirt(String newColor, char newSize) {
        color = newColor;
        size = newSize;
    }


    public static void putON() {
        System.out.println("Shirt is on");
    }

    public static void takeOFF() {
        System.out.println("Shirt is off");
    }

    public static void setColor(String newColor) {
        color = newColor;
    }

    public static void setSize(char newSize) {
        size = newSize;
    }

}
