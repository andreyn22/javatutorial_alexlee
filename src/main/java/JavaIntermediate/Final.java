package JavaIntermediate;

public class Final {
    final int MAX = 5;
//    void changeMax(int newMax){
//        MAX = newMax;
//    } this can work only if we don't have the final value for MAX

    public static void main(String[] args) {
        Final f = new Final();
        System.out.println(f.MAX);
        f.sayHi();
    }

    final public void sayHi() {
        System.out.println("Hi");
    }

//    class OtherClass extends Final {
//        public void sayHi() { // we can't override a final method
//            System.out.println("Hello");
//        }
//    }
}
