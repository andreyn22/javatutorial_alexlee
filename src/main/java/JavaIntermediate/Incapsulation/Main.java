package JavaIntermediate.Incapsulation;

public class Main {
    public static void main(String[] args) {
        Student s = new Student();
//        s.name = "Tommy";
//        s.age = 17;
//        System.out.println(s.name);
//        System.out.println(s.age); we use incapsulation through getters&setters

        s.setName("Suzie");
        s.setAge(24);
        System.out.println(s.getName());
        System.out.println(s.getAge());
    }
}
