package JavaIntermediate.Inherit;

public class Main {
    public static void main(String[] args) {
        Mouse1 m1 = new Mouse1();


        m1.leftClick();
        m1.rightClick();
        m1.scroolDown();
        m1.scroolUp();


        Mouse2 m2 = new Mouse2();
        m2.connect();
    }
}
