package JavaIntermediate.Inherit;

public class Mouse {
    public static void leftClick() {
        System.out.println("Click left");
    }

    public static void rightClick() {
        System.out.println("Click right");
    }

    public static void scroolUp() {
        System.out.println("Scrolled up !");
    }

    public static void scroolDown() {
        System.out.println("Scrolled down");
    }
}
