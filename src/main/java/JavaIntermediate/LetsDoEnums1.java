package JavaIntermediate;

//Enum = class + an array => list of values that doesn't change, but is setup like a class

enum Level {
    LOW, MEDIUM, HIGH;
}

public class LetsDoEnums1 {

    public static void main(String[] args) {
        Level l = Level.LOW;
        System.out.println(l);

        switch (l) {
            case LOW:
                System.out.println("Low level");
                break;
            case MEDIUM:
                System.out.println("Medium level");
                break;
            case HIGH:
                System.out.println("High level");
        }
    }
}
