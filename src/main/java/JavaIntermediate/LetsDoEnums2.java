package JavaIntermediate;

//Enum = class + an array => list of values that doesn't change, but is setup like a class


public class LetsDoEnums2 {
    public static void main(String[] args) {
        Flavor flav = Flavor.VANILLA;

        if (flav == flav.VANILLA) {
            System.out.println("it's vanilla");
        } else if (flav == flav.CHOCOLATE) {
            System.out.println("it's chocolate");
        } else if (flav == flav.STRAWBERRY) {
            System.out.println("it's strawberry");
        }

    }

    enum Flavor {
        CHOCOLATE, VANILLA, STRAWBERRY;
    }
}
