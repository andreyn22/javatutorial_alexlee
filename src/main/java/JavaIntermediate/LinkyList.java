package JavaIntermediate;

import java.util.LinkedList;

public class LinkyList {
    public static void main(String[] args) {
        LinkedList<String> linky1 = new LinkedList<String>();
        linky1.add("Rob");
        linky1.add("Alex");
        linky1.add("Andrei");

        System.out.println(linky1);
        linky1.remove("Rob");
        System.out.println(linky1.get(1));


        LinkedList<Integer> linky2 = new LinkedList<Integer>();
        linky2.add(6);
        linky2.add(78);
        linky2.add(1);

        System.out.println(linky2.get(1));
    }
}

