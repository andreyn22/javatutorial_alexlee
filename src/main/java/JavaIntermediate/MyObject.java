package JavaIntermediate;

interface Bird {
    public void sing();
}

public class MyObject {
    public static void main(String[] args) {
        MyObject o = new MyObject();
        System.out.println(o instanceof MyObject);
        //class, subclass or interface

        Eagle e1 = new Eagle();
        System.out.println(e1 instanceof Bird);
//        Eagle e2 = null; // null means there's nothing there
//        System.out.println(e2 instanceof Bird); // false, there's no contsructor to make the object.


    }
}

class Eagle implements Bird {

    public void sing() {
        System.out.println("Singin!");

    }
}

