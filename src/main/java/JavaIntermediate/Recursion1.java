package JavaIntermediate;

// Recursion is the technique of making a function call itself
// The rule for the recursion is that need to know when to stop

public class Recursion1 {
    public static void main(String[] args) {
//        sayHi();
    }

    public static void sayHi() {
        System.out.println("Hi");
        sayHi();
    }

// Need a base case
}
