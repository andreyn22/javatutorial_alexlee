package JavaIntermediate;

// Recursion is the technique of making a function call itself
// The rule for the recursion is that need to know when to stop

public class Recursion2 {
    public static void main(String[] args) {
        sayHi(5);
    }

    public static void sayHi(int n) {
        if (n == 0) {
            System.out.println("Done");
        } else {
            System.out.println("Hi");
            n--;
            sayHi(n);
        }
    }

// Need a base case
}
