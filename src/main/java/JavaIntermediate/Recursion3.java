package JavaIntermediate;

// Recursion is the technique of making a function call itself
// The rule for the recursion is that need to know when to stop

public class Recursion3 {
    public static void main(String[] args) {
        countBackwards(14);
    }

    public static void countBackwards(int n) {
        if (n == 0) {
            System.out.println("Done");
        } else {
            System.out.println(n);
            n--;
            countBackwards(n);
        }
    }

// Need a base case
}
