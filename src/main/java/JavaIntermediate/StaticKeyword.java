package JavaIntermediate;

public class StaticKeyword {
    //     int a = 0;  // global variable
    static int a = 0;

    public static void main(String[] args) {
        int b = 1; // static variable

        StaticKeyword s = new StaticKeyword();
        System.out.println(b);


//        System.out.println(s.a);
        System.out.println(a); // static helps to get the global variable and use it in main
    }
}
