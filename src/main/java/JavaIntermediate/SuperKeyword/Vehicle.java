package JavaIntermediate.SuperKeyword;

class Vehicle {
    int maxSpeed = 120;

    public void vroom() {
        System.out.println("Vroom vroom");
    }

}

class Car extends Vehicle {
    int maxSpeed = 100;

    public void display() {
        System.out.println(super.maxSpeed);   //  max speed from superclass
        System.out.println(maxSpeed);         // max speed from the childclass
    }

/*    public void vroom() {
        System.out.println("Skuuurt");
    }*/

    public void vroom() {
        super.vroom();
    }

}

class Main {
    public static void main(String[] args) {
        Car c = new Car();
        c.display();
        c.vroom();
    }
}
