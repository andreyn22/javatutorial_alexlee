package JavaIntermediate.SuperWithConstructors;

class Vehicle {
    int maxSpeed;

    Vehicle() {
        System.out.println("Vehicle constructor");
    }
}

class Car extends Vehicle {
    int maxSpeed;

    Car() {
        super(); // super has to be first when calling the method of the firstclass
        System.out.println(("Car constructor"));
    }
}

class Main {
    public static void main(String[] args) {
        Car c = new Car();
    }
}
