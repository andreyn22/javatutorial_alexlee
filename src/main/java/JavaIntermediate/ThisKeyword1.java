package JavaIntermediate;

public class ThisKeyword1 {

    int c;
    int d;

    public static void main(String[] args) {
        ThisKeyword1 t = new ThisKeyword1();
        t.setData(4, 3);
        System.out.println(t.c);
        System.out.println(t.d);
    }

    public void setData(int a, int b) {
        this.c = a;
        this.d = b;
    }
}
