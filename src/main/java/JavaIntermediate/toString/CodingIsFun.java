package JavaIntermediate.toString;

public class CodingIsFun {
    public static void main(String[] args) {
        Student s1 = new Student(1, "Bruce");
        Student s2 = new Student(2, "Jack");

        System.out.println(s1);
        System.out.println(s2);
    }
}
