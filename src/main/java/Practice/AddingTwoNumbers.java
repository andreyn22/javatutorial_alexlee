package Practice;

import java.util.Scanner;

public class AddingTwoNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a number:");
        int firstNumber = scan.nextInt();

        System.out.println("Enter another number:");
        int secondNumber = scan.nextInt();

        System.out.println("The sum of those two numbers is: " + (firstNumber + secondNumber));
    }
}
