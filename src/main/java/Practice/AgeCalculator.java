package Practice;

import java.time.LocalDate;
import java.time.Period;

public class AgeCalculator {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate birthDate = LocalDate.of(1990, 4, 15);
        int yearsAge= Period.between(birthDate, today).getYears();
        int monthsAge = Period.between(birthDate, today).getMonths();

        System.out.println(today);
        System.out.println(birthDate);
        System.out.println(yearsAge);
        System.out.println(monthsAge);
    }
}
