package src.main.java.Practice;

public class CapitalizeProgram {
    public static void main(String[] args) {
        String s = "happy birthday";
        s.toUpperCase();
        System.out.println(s.toUpperCase());
        System.out.println(s.substring(0, 1).toUpperCase());

        String result = s.substring(0, 1).toUpperCase() + s.substring(1);
        System.out.println(result);

    }
}
