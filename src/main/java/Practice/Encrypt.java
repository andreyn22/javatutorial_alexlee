package src.main.java.Practice;

public class Encrypt {
    public static void main(String[] args) {

        int key = 6;

// encryption
        String text = "Hey, how's it hangin?";
        System.out.println(text);

        char[] chars = text.toCharArray();

        for (char c : chars) {
            c += key;  //https://www.asciitable.com/
            System.out.print(c);
        }

        System.out.println();
        System.out.println();

// decryption
        String text2 = "Nk\u007F2&nu}-y&oz&ngtmotE";
        System.out.println(text2);
        char[] chars2 = text2.toCharArray();

        for (char c2 : chars2) {
            c2 -= key;  //https://www.asciitable.com/
            System.out.print(c2);
        }

    }
}
