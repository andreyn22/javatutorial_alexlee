package src.main.java.Practice;

import java.util.Scanner;

public class Growth {
    public static void main(String[] args) {
        // y=a(1+r)^x
        // a = initial value
        // r= growth rate
        // x- time interval

        System.out.println("Let's calculate subs!");
        System.out.println("This program uses = a(1+r)^x");

        System.out.println("Enter initial value(a): ");
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();

        System.out.println("Enter growth rate (r): ");
        double r = scan.nextDouble();

        System.out.println("Enter time interval in days (x): ");
        int x = scan.nextInt();

        System.out.println(calculateSubs(a, r, x));
    }

    public static int calculateSubs(int a, double r, int x) {
        //        return a(1+r)^x; since in java we don't have ^, we use:
        return (int) (a * Math.pow((1 + r), x));

    }
}

//    Enter initial value(a):
//        35000
//        Enter growth rate (r):
//        0.024
//        Enter time interval in days (x):
//        4
//        38482