package Practice;

import java.util.Random;

public class RollingDice {
    public static void main(String[] args) {
        Random coolNumberRandom = new Random();
        int x = coolNumberRandom.nextInt(6) + 1;

        System.out.println("You rolled a: " + x);
    }
}
