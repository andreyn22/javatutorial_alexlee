package package1;

public class Access1 {
//    default
//    public // everyone can see the data
//    private  = default // only in the same package
//    protected // in the same package or extended classes

    int hours1 = 3; //default
    int minutes1 = 47; //default

    public int hours2 = 5;
    public int minutes2 = 10;

    protected int hours3 = 5;
    protected int minutes3 = 10;
}
